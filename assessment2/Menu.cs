﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace assessment2
{
    /// Author: Man Kit Kyle Cheng
    /// Description: Menu Class
    /// Date Last Modified: 11/12/2015

    public class Menu
    {
        private string Description { get; set; } 

        public string Item_Description
        {
            get { return Description; }
            set
            {
                if (Description == null)
                {
                    throw new Exception("Please enter a description");
                }
                else
                {
                    Description = value;
                }

            }
        }
        private bool Vegetarian { get; set; }

        public bool Item_Vegetarian
        {
            get { return Vegetarian; }
            set
            {
                if (Vegetarian == true)
                {
                    Vegetarian = value;
                }
                else 
                {
                    Vegetarian = false;
                }
            }
        }
        private int Price { get; set; }
        public int Item_Price
        {
            get { return Price; }
            set
            {   
                if (Price > 100000) 
                {
                    throw new Exception("Please enter the correct value");//This shows a new message to the user if the value is greater than 100000
                }
                else if (Price < 0)
                {
                    throw new Exception("Please enter the correct value");//This shows a new message to the user if the value is less than 00
                }
                else
                {
                    Price = value;
                }
            }
        }
        private int Table { get; set; }
        public int Item_Table
        {
            get { return Table; }
            set
            {
                if (Table > 10)
                {
                    throw new Exception("Please enter the correct value");
                }
                else if (Table <= 0)
                {
                    throw new Exception("Please enter the correct value");
                }
                else 
                {
                    Table = value;
                }
            }
        }

        List<Menu> items = new List<Menu>();

        private string Customer_Name { get; set; }
        public string Item_Customer_Name
        {
            get { return Customer_Name; }
            set
            {
                if (Customer_Name == null)
                {
                    throw new Exception("Please enter the correct value");
                }
                else
                {
                    Customer_Name = value;
                }
            }
        }
        private string Delivery_Address { get; set; }
        public string Item_Delivery_Address
        {
            get { return Delivery_Address; }
            set
            {
                if (Delivery_Address == null)
                {
                    throw new Exception("Please enter the correct value");
                }
                else
                {
                    Delivery_Address = value;
                }
            }
        }

      
    
        




    }
        
    
}