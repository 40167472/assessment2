﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// Author: Man Kit Kyle Cheng
    /// Description: Main Window
    /// Date Last Modified: 11/12/2015
    public partial class MainWindow : Window
    {
        //public DropDownList();
        int table;
        int driver;



        public MainWindow()
        {
            InitializeComponent();

            //Manager listbox = new Manager(this);
            //listbox.txtServerName = txtBill;

            //listbox.txtPrice = txtBill;

        }
        private void btnManager_Click(object sender, RoutedEventArgs e)
        {
            Manager man = new Manager(this);
            man.Show(); //Shows the Manager window
           // Close();

        }

        private void listBoxDishes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //listBoxDishes.Items.Add(txtCustomerName.Text);
            //listBoxDishes.Items.Add(radioButtonSit_in);
            //listBoxDishes.Items.Add(txtTableNumber.Text);
            //listBox.Items.Add(txtServerName.Text);
            //listBox.Items.Add(txtPrice.Text);

            //listBoxDishes.Items.Add(radioButtonDelivery_order);
            //listBoxDishes.Items.Add(txtDeliveryAddress.Text);
            //listBoxDishes.Items.Add(listBoxDrivers);

            
        }

        private void btnBill_Click(object sender, RoutedEventArgs e)
        {
            
            Manager mr = new Manager(this);
            mr.listBoxMenuItems = listBoxBill;
            
            listBoxBill.Items.Add(txtCustomerName.Text + " " + txtDeliveryAddress.Text + " "+ txtTableNumber.Text + " " + mr.txtDescription.Text + " " + mr.txtPrice.Text);//This will display the customer name, delivery address or table number in the drivers list box
            // ==listBoxBill.Items.Add(txtTableNumber.Text)
            //if (table <= 10)
            //{
            //    MessageBox.Show("Within the range"); //This will show a message box that the table is within the range
            //}
            //else if(table > 10)
            //{
            //    MessageBox.Show("Please enter the correct number in the range"); //This will show an error message if the server entered a table number above ten.
            //}
            //else if (table < 0)
            //{
            //    MessageBox.Show("Please enter the correct number in the range"); //This will show an error message if the server entered a table number below zero.
            //}
        }

        private void radioButtonSit_in_Checked(object sender, RoutedEventArgs e)
        {
            if (table < 11)
            {

            }

            else if (table < 0)
            {
                MessageBox.Show("Please enter the correct number in the range"); //This will show an error message if the server entered a table number below zero.
            }

            else if (table > 10)
            {
                MessageBox.Show("Please enter the correct number in the range"); //This will also display an error message if the server entered a table number above ten.
            }
        }

        private void radioButtonDelivery_order_Checked(object sender, RoutedEventArgs e)
        {
            if (driver < 0)
                {
                    throw new Exception("Please enter the correct data");
                }
        }

        private void comboBoxServer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            Manager manager = new Manager(this);
            //manager.txtServerName.Text = comboBoxServer.ToString();
            manager.listBoxMenuItems.Items.Add(comboBoxServer.ToString());
            //Manager ma = new Manager(this);
            //ma.txtServerName.Text = comboBoxServer;
            //main.listBoxDishes.Items.Add(txtDescription.Text);
            //main.txtBill.Text = txtDescription.Text + " " + txtPrice.Text;

            //object description = txtDescription.Text + " " + txtPrice.Text + " " + checkBox.IsChecked;

            //listBoxMenuItems.Items.Add(txtDescription.Text + " " + txtPrice.Text + " " + checkBox.IsChecked);

        }

        public void CopyDishes()
        {
            Manager man = new Manager(this);
            foreach (var item in man.listBoxMenuItems.Items)
            {
                listBoxDishes.Items.Add(item.ToString());
            }
        }

        private void btnClearDishes_Click(object sender, RoutedEventArgs e)
        {
            listBoxDishes.Items.Remove(listBoxDishes.SelectedItem);//This will clear the selected items in the dishes ordered list
            listBoxBill.Items.Remove(listBoxBill.SelectedItem);//This will remove the selected items in the bill list
        }

        private void btnDriverList_Click(object sender, RoutedEventArgs e)
        {
            listBoxDrivers.Items.Remove(listBoxDrivers.SelectedItem);
        }


        
    }
}
