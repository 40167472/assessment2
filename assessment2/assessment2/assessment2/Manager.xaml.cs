﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for Manager.xaml
    /// </summary>
    /// Author: Man Kit Kyle Cheng
    /// Description: Manager Window
    /// Date Last Modified: 11/12/2015
    public partial class Manager : Window
    {
        MainWindow main;
        public Manager(MainWindow outside)
        {
            InitializeComponent();
            main = outside;
        }

        private void listBoxMenuItems_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            main.listBoxDishes.Items.Add(txtDescription.Text );
            main.listBoxBill.Items.Add(txtDescription.Text + " " + txtPrice.Text);
            
            object description = txtDescription.Text + " " + txtPrice.Text + " " + checkBox.IsChecked;//This will link all the text boxes together

            listBoxMenuItems.Items.Add(txtDescription.Text + " " + txtPrice.Text + " " + checkBox.IsChecked); //This will add the description, price and vegetarian checkbox to the list box
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            listBoxMenuItems.Items.Remove(listBoxMenuItems.SelectedItem); //This will remove the description, price and vegetarian checkbox from the list box
            
        }

        private void btnAmend_Click(object sender, RoutedEventArgs e)
        {
            object description = txtDescription.Text + " " + txtPrice.Text + " " + checkBox.IsChecked;//This will link all the text boxes together

            listBoxMenuItems.Items.Remove(listBoxMenuItems.SelectedItem);//Removes the selected item from the menu items list box
            listBoxMenuItems.Items.Add(description);//This will add the description to the removed selected item in the menu item list box
        }

        private void btnAdd1_Click(object sender, RoutedEventArgs e)
        {
            object ServersDescription = txtServerName.Text + " " + txtServerID.Text;//This will link all the text boxes together

            listBoxServers.Items.Add(txtServerName.Text + " " + txtServerID.Text); //This will add the server name and server id to the list box

            MainWindow mwindow = new MainWindow();
            mwindow.comboBoxServer.Items.Add(txtServerName.Text);//This will link the data from the server name text box to the main window combo box
        }

        private void btnRemove1_Click(object sender, RoutedEventArgs e)
        {
            listBoxServers.Items.Remove(listBoxServers.SelectedItem); //This will remove the server name and server id from the list box
        }

        private void btnAmend1_Click(object sender, RoutedEventArgs e)
        {
            object ServersDescription = txtServerName.Text + " " + txtServerID.Text;//This will link all the text boxes together

            listBoxServers.Items.Remove(listBoxMenuItems.SelectedItem);//Removes the selected item from the menu items list box
            listBoxServers.Items.Add(ServersDescription);//This will add the server description to the removed selected item in the menu item list box
        }

        private void btnAdd2_Click(object sender, RoutedEventArgs e)
        {
            object DriverDescription = txtDriverName.Text + " " + txtDriverID.Text + " " + txtCarReg.Text;//This will link all the text boxes together

            listBoxDrivers.Items.Add(txtDriverName.Text + " " + txtDriverID.Text + " " + txtCarReg.Text); //This will add the driver name, driver id and car reg to the list box

            main.listBoxDrivers.Items.Add(txtDriverName.Text);//This will the driver's name on the drivers list box
        }

        private void btnRemove2_Click(object sender, RoutedEventArgs e)
        {
            listBoxDrivers.Items.Remove(listBoxDrivers.SelectedItem); //This will remove the driver name, driver id and car reg from the list box
            //txtDriverName.Text + " " + txtDriverID.Text + " " + txtCarReg.Text
        }

        private void btnAmend2_Click(object sender, RoutedEventArgs e)
        {
            object DriverDescription = txtDriverName.Text + " " + txtDriverID.Text + " " + txtCarReg.Text;//This will link all the text boxes together

            listBoxDrivers.Items.Remove(listBoxDrivers.SelectedItem);//Removes the selected item from the menu items list box
            listBoxDrivers.Items.Add(DriverDescription);//This will add the driver description to the removed selected item in the menu item list box


        }

        private void listBoxServers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void listBoxDrivers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnBack2MainWindow_Click(object sender, RoutedEventArgs e)
        {
            Close(); //This will close the manager window
            main.CopyDishes();//Links back the data back to the main window 
        }
    }
}
