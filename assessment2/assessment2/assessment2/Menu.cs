﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace assessment2
{
    /// Author: Man Kit Kyle Cheng
    /// Description: Menu Class
    /// Date Last Modified: 11/12/2015

    public class Menu
    {
        private string Description { get; set; } 

        public string Item_Description
        {
            get { return Description; }
            set
            {
                if (Description == null)
                {
                    throw new Exception("Please enter a description");//This will throw an error message to the user if there is no description value
                }
                else
                {
                    Description = value;//This should display the description
                }

            }
        }
        private bool Vegetarian { get; set; }

        public bool Item_Vegetarian
        {
            get { return Vegetarian; }
            set
            {
                if (Vegetarian == true)
                {
                    Vegetarian = value;//This will happen if the check box has been checked
                }
                else 
                {
                    Vegetarian = false;//This will happen if the check box hasn't been checked
                }
            }
        }
        private int Price { get; set; }
        public int Item_Price
        {
            get { return Price; }
            set
            {   
                if (Price > 100000) 
                {
                    throw new Exception("Please enter the correct value");//This shows a new message to the user if the value is greater than 100000
                }
                else if (Price < 0)
                {
                    throw new Exception("Please enter the correct value");//This shows a new message to the user if the value is less than 00
                }
                else
                {
                    Price = value;//This should display the correct value
                }
            }
        }
        private int Table { get; set; }
        public int Item_Table
        {
            get { return Table; }
            set
            {
                if (Table > 10)
                {
                    throw new Exception("Please enter the correct value");//The error message will come up if the table number is above 10
                }
                else if (Table <= 0)
                {
                    throw new Exception("Please enter the correct value");//The error message will come up if the table number is 0 or less
                }
                else 
                {
                    Table = value;//This will display the correct value
                }
            }
        }

        List<Menu> items = new List<Menu>();

        private string Customer_Name { get; set; }
        public string Item_Customer_Name
        {
            get { return Customer_Name; }
            set
            {
                if (Customer_Name == null)
                {
                    throw new Exception("Please enter the correct value");//This will display an error message if there is no value in the customer name text box
                }
                else
                {
                    Customer_Name = value;//This will display the correct value
                }
            }
        }
        private string Delivery_Address { get; set; }
        public string Item_Delivery_Address
        {
            get { return Delivery_Address; }
            set
            {
                if (Delivery_Address == null)
                {
                    throw new Exception("Please enter the correct value");//This will throw an error message at the user if there is no delivery address data in the delivery address value
                }
                else
                {
                    Delivery_Address = value;//This will show the if there is a data in the deliver address value
                }
            }
        }

      
    
        




    }
        
    
}