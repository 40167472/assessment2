﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// Author: Man Kit Kyle Cheng
    /// Description: Main Window
    /// Date Last Modified: 11/12/2015
    public partial class MainWindow : Window
    {
        //Variables
        int table;
        int driver;



        public MainWindow()
        {
            InitializeComponent();
        }
        private void btnManager_Click(object sender, RoutedEventArgs e)
        {
            Manager man = new Manager(this); //This will link the main window to the manager window
            man.Show(); //Shows the Manager window
           

        }

        private void listBoxDishes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void btnBill_Click(object sender, RoutedEventArgs e)
        {
            
            Manager mr = new Manager(this); 
            mr.listBoxMenuItems = listBoxBill; //This will link the data from the manager window menu items list box to the main window bill list box
            
            listBoxBill.Items.Add(txtCustomerName.Text + " " + txtDeliveryAddress.Text + " "+ txtTableNumber.Text + " " + mr.txtDescription.Text + " " + mr.txtPrice.Text);//This will display the customer name, delivery address or table number in the drivers list box
        }

        private void radioButtonSit_in_Checked(object sender, RoutedEventArgs e)
        {
            
        }

        private void radioButtonDelivery_order_Checked(object sender, RoutedEventArgs e)
        {
            
        }

        private void comboBoxServer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            Manager manager = new Manager(this);
            manager.txtServerName.Text = comboBoxServer.ToString(); //The combo box should produce data from the server name text box in the manager window
        }

        public void CopyDishes()
        {
            Manager man = new Manager(this);
            foreach (var item in man.listBoxMenuItems.Items) //This foreach loop does this if there is an item
            {
                listBoxDishes.Items.Add(item.ToString()); //Adds the menu item from the manager window to the dishes list box
            }
        }

        private void btnClearDishes_Click(object sender, RoutedEventArgs e)
        {
            listBoxDishes.Items.Remove(listBoxDishes.SelectedItem);//This will clear the selected items in the dishes ordered list
            listBoxBill.Items.Remove(listBoxBill.SelectedItem);//This will remove the selected items in the bill list
        }

        private void btnDriverList_Click(object sender, RoutedEventArgs e)
        {
            listBoxDrivers.Items.Remove(listBoxDrivers.SelectedItem);//This will remove any selected item in the drivers list box  
        }

        private void txtTableNumber_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
