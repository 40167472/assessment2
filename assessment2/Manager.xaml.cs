﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for Manager.xaml
    /// </summary>
    /// Author: Man Kit Kyle Cheng
    /// Description: Manager Window
    /// Date Last Modified: 11/12/2015
    public partial class Manager : Window
    {
        MainWindow main;
        public Manager(MainWindow outside)
        {
            InitializeComponent();
            main = outside;
        }

       

        private void listBoxMenuItems_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //MainWindow mw = new MainWindow();
            //mw.txtBill.Text = txtDescription.Text;

            

            //listBoxMenuItems.Items.Add(mw);
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            //MainWindow mw = new MainWindow();
            main.listBoxDishes.Items.Add(txtDescription.Text );
            main.listBoxBill.Items.Add(txtDescription.Text + " " + txtPrice.Text);
            //main.btnBill = ;
            //txtBill.Text + txtDescription.Text + " " + txtPrice.Text;
            object description = txtDescription.Text + " " + txtPrice.Text + " " + checkBox.IsChecked;

            listBoxMenuItems.Items.Add(txtDescription.Text + " " + txtPrice.Text + " " + checkBox.IsChecked); //This will add the description, price and vegetarian checkbox to the list box
                                                                                                              
            //MainWindow mains = new MainWindow();
            //mains.comboBoxServer.Items.Add(txtServerName.Text);
            //if 
            //{

            //}           

            //try
            //{

            //}
            //catch(Exception message)
            //{
            //    MessageBox.Show(message.Message);
            //}
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            listBoxMenuItems.Items.Remove(listBoxMenuItems.SelectedItem); //This will remove the description, price and vegetarian checkbox from the list box
            
        }

        private void btnAmend_Click(object sender, RoutedEventArgs e)
        {
            //listBox.Items.Remove(txtDescription.Text);
            //listBox.Items.Remove(txtPrice.Text);
            //listBox.Items.Remove(checkBox.IsChecked);
            //listBoxMenuItems.Items.RemoveAt(0);
            //listBoxMenuItems.Items.Add(txtDescription.Text);
            //listBoxMenuItems.Items.Add(txtPrice.Text);
            //listBoxMenuItems.Items.Add(checkBox.IsChecked);
            //MainWindow mw = new MainWindow();
            //mw.listBoxDishes = listBoxMenuItems;

            object description = txtDescription.Text + " " + txtPrice.Text + " " + checkBox.IsChecked;

            listBoxMenuItems.Items.Remove(listBoxMenuItems.SelectedItem);
            listBoxMenuItems.Items.Add(description);

            //listBoxMenuItems.Items.Add(mw.listBoxDishes =listBoxMenuItems);
        }

        private void btnAdd1_Click(object sender, RoutedEventArgs e)
        {
            object ServersDescription = txtServerName.Text + " " + txtServerID.Text;

            listBoxServers.Items.Add(txtServerName.Text + " " + txtServerID.Text); //This will add the server name and server id to the list box

            MainWindow mwindow = new MainWindow();
            mwindow.comboBoxServer.Items.Add(txtServerName.Text);
        }

        private void btnRemove1_Click(object sender, RoutedEventArgs e)
        {
            listBoxServers.Items.Remove(listBoxServers.SelectedItem); //This will remove the server name and server id from the list box
            //txtServerName.Text + " " + txtServerID.Text
        }

        private void btnAmend1_Click(object sender, RoutedEventArgs e)
        {
            //listBox.Items.Refresh();
            //listBoxServers.Items.RemoveAt(0);
            //listBoxServers.Items.Add(txtServerName.Text + txtServerID.Text);
            //listBox.Items.Add(txtServerID.Text);

            object ServersDescription = txtServerName.Text + " " + txtServerID.Text;

            listBoxServers.Items.Remove(listBoxMenuItems.SelectedItem);
            listBoxServers.Items.Add(ServersDescription);
        }

        private void btnAdd2_Click(object sender, RoutedEventArgs e)
        {
            object DriverDescription = txtDriverName.Text + " " + txtDriverID.Text + " " + txtCarReg.Text;

            listBoxDrivers.Items.Add(txtDriverName.Text + " " + txtDriverID.Text + " " + txtCarReg.Text); //This will add the driver name, driver id and car reg to the list box

            main.listBoxDrivers.Items.Add(txtDriverName.Text);//This will the driver's name on the drivers list box
        }

        private void btnRemove2_Click(object sender, RoutedEventArgs e)
        {
            listBoxDrivers.Items.Remove(listBoxDrivers.SelectedItem); //This will remove the driver name, driver id and car reg from the list box
            //txtDriverName.Text + " " + txtDriverID.Text + " " + txtCarReg.Text
        }

        private void btnAmend2_Click(object sender, RoutedEventArgs e)
        {
            //listBox.Items.Refresh();
            //listBoxDrivers.Items.Remove(listBoxDrivers.SelectedValue);
            //listBoxDrivers.Items.Add(txtDriverName.Text);
            //listBoxDrivers.Items.Add(txtDriverID.Text);
            //listBoxDrivers.Items.Add(txtCarReg.Text);
            //listBoxDrivers.Items.Remove();
            

            object DriverDescription = txtDriverName.Text + " " + txtDriverID.Text + " " + txtCarReg.Text;

            listBoxDrivers.Items.Remove(listBoxDrivers.SelectedItem);
            listBoxDrivers.Items.Add(DriverDescription);

            
        }

        private void listBoxServers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void listBoxDrivers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnBack2MainWindow_Click(object sender, RoutedEventArgs e)
        {
            Close(); //This will close the manager window
            main.CopyDishes();

            //MainWindow mw = new MainWindow();
            //mw.listBoxDishes.Items.Add(txtDescription.Text + " " + txtPrice.Text);
            //mw.comboBoxServer.Items.Add(txtServerName.Text);
            
        }
    }
}
